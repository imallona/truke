#!/bin/bash
##
## This file is part of truke.
##
## truke is free software: you can redistribute it and/or modify
## it under the terms of the GNU General Public License as published by
## the Free Software Foundation, either version 3 of the License, or
## (at your option) any later version.
##
## truke is distributed in the hope that it will be useful,
## but WITHOUT ANY WARRANTY; without even the implied warranty of
## MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
## GNU General Public License for more details.
##
## You should have received a copy of the GNU General Public License
## along with truke.  If not, see <http://www.gnu.org/licenses/>.
##
## Izaskun Mallona, 2016
##
##
## cron suggested settings:

HOME='/home/labs/maplab/imallona'
WD=${HOME}'/symbols'
REPORTING_EMAIL="tools@maplab.cat"
REGEX="/^(JAN|FEB|MAR|APR|MAY|JUN|JUL|AUG|SEP|SEPT|OCT|NOV|DEC|Jan|Feb|Mar|Apr|May|Jun|Jul|Aug|Sep|Oct|Nov|Dec|jan|feb|mar|apr|may|jun|jul|aug|sep|sept|oct|nov|dec)(-)?(0?[1-9]|1[0-9]|2[0-9]|3[01])$/"

mkdir -p ${WD}
cd ${WD}

echo 'Copying preexisting gene symbols, if present'

if [ -f human_symbols ]
then
    mv human_symbols human_symbols_old
fi


if [ -f non_human_symbols ]
then
    mv non_human_symbols non_human_symbols_old
fi

echo 'Data download'

if [ -f gene_info.gz ]
then
    mv gene_info.gz gene_info_old.gz
   
fi

if [ -f gene_info ]
then
    mv gene_info gene_info_old
fi

wget  --no-verbose \
      --header="Accept: text/html" \
      --user-agent="Mozilla/5.0 (Macintosh; Intel Mac OS X 10.8; rv:21.0) Gecko/20100101 Firefox/21.0" \
      ftp://ftp.ncbi.nlm.nih.gov/gene/DATA/gene_info.gz
# wget ftp://ftp.ebi.ac.uk/pub/databases/genenames/new/tsv/hgnc_complete_set.txt


if [[ -f gene_info.gz && -f gene_info_old.gz ]]
then
    gene_info_diff=$(zdiff gene_info.gz gene_info_old.gz)
    if [ "$gene_info_diff" == "" ]
    then
        ## no differences, so exit
        echo 'No differences, done'
        exit 0
    fi
fi

echo 'Data parsing since the gene info file changed from last time'

gunzip gene_info.gz

## human taxids
grep -c -w '^9606' gene_info        
# 60119
grep -c -w -v '^9606' gene_info
# 16455156

## 9606 is the 'Homo sapiens' taxid
## gene info cols f5 and f3 contain gene symbols and accepted synonyms



grep -w '^9606' gene_info | \
    cut -f5 | \
    tr '|' '\n' | \
    awk '/^(JAN|FEB|MAR|APR|MAY|JUN|JUL|AUG|SEP|SEPT|OCT|NOV|DEC|Jan|Feb|Mar|Apr|May|Jun|Jul|Aug|Sep|Oct|Nov|Dec|jan|feb|mar|apr|may|jun|jul|aug|sep|sept|oct|nov|dec)(-)?(0?[1-9]|1[0-9]|2[0-9]|3[01])$/' | uniq > human_symbols

grep -w -v '^9606' gene_info | \
    cut -f5 | \
    tr '|' '\n' | \
    awk '/^(JAN|FEB|MAR|APR|MAY|JUN|JUL|AUG|SEP|SEPT|OCT|NOV|DEC|Jan|Feb|Mar|Apr|May|Jun|Jul|Aug|Sep|Oct|Nov|Dec|jan|feb|mar|apr|may|jun|jul|aug|sep|sept|oct|nov|dec)(-)?(0?[1-9]|1[0-9]|2[0-9]|3[01])$/' | uniq > non_human_symbols

grep -w '^9606' gene_info | \
    cut -f3 | \
    tr '|' '\n' | \
    awk '/^(JAN|FEB|MAR|APR|MAY|JUN|JUL|AUG|SEP|SEPT|OCT|NOV|DEC|Jan|Feb|Mar|Apr|May|Jun|Jul|Aug|Sep|Oct|Nov|Dec|jan|feb|mar|apr|may|jun|jul|aug|sep|sept|oct|nov|dec)(-)?(0?[1-9]|1[0-9]|2[0-9]|3[01])$/' | uniq >> human_symbols

grep -w -v '^9606' gene_info | \
    cut -f3 | \
    tr '|' '\n' | \
    awk '/^(JAN|FEB|MAR|APR|MAY|JUN|JUL|AUG|SEP|SEPT|OCT|NOV|DEC|Jan|Feb|Mar|Apr|May|Jun|Jul|Aug|Sep|Oct|Nov|Dec|jan|feb|mar|apr|may|jun|jul|aug|sep|sept|oct|nov|dec)(-)?(0?[1-9]|1[0-9]|2[0-9]|3[01])$/' | uniq >> non_human_symbols

awk '{print toupper($0)}' human_symbols | sort  | uniq > foo
mv foo human_symbols

awk '{print toupper($0)}' non_human_symbols | sort  | uniq > foo
mv foo non_human_symbols

cat human_symbols non_human_symbols | sort | uniq > complete_symbols

echo 'Checking whether there has been an update of the Excel-corruptable gene symbols'

msg="void"

if [ -f human_symbols ]
then
    if [ -f human_symbols_old ]
    then
        
        human_diff=$(diff  human_symbols human_symbols_old) 
        if [ "$human_diff" != "" ] 
        then
            msg="Human gene symbols updated, check truke"
        fi
    else
        msg='Retrieving human gene symbols for the first time'
    fi
fi

if [ -f non_human_symbols ]
then
    if [ -f non_human_symbols_old ]
    then
        
        non_human_diff=$(diff  non_human_symbols non_human_symbols_old) 
        if [ "$non_human_diff" != "" ]
        then
            msg="Non human gene symbols updated, check truke"
        fi
    else
        msg='Retrieving non human gene symbols for the first time'
    fi
fi

## either human or non human has changed
if [ "$msg" != "void" ]
then
    echo $msg | mail -s "Gene symbols updated" "$REPORTING_EMAIL"
fi

echo $msg

echo Done
exit 0
